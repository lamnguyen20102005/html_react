import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.js'; // Update the import statement to 'App.js'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
