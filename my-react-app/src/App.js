import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import './App.css';

class App extends React.Component {
  validateForm(event) {
    event.preventDefault();
    const username = document.getElementById('uname').value;
    const password = document.getElementById('psw').value;
    if (username === 'Lam' && password === '123') {
      alert('Login successful!');
      window.location.href = 'https://www.youtube.com';
    } else {
      alert('Invalid username or password. Please try again.');
    }
  }

  openEmployeesPage(name) {
    // Implementation for opening Employees tab
  }

  changeEmailLinkColor(event, email) {
    event.preventDefault();
    const emailLink = event.target;
    emailLink.classList.add('clicked');
    emailLink.href = 'mailto:' + email;
  }

  render() {
    return (
      <div className="container">
        <div className="center-content">
        <h1 className="title">My Company</h1>
        <p className="sub-title">Click on the button to open the tabs:</p>
        </div>
        <Tabs>
          <TabList>
            <Tab>Login</Tab>
            <Tab>Information</Tab>
            <Tab>Employees Information</Tab>
            <Tab>Manager Information</Tab>
          </TabList>

          <TabPanel>
            <h3>Login</h3>
            <form onSubmit={this.validateForm}>
              <label htmlFor="uname"><b>Username</b></label>
              <input type="text" placeholder="Enter Username" name="uname" id="uname" required />

              <label htmlFor="psw"><b>Password</b></label>
              <input type="password" placeholder="Enter Password" name="psw" id="psw" required />

              <button type="submit">Login</button>
            </form>
          </TabPanel>

          <TabPanel>
            <h3>Information</h3>
            <div className="info-box">
              <label><b>Name:</b></label>
              <p>Lam Nguyen</p>

              <label><b>Phone Number:</b></label>
              <p>5712754697</p>

              <label><b>Email:</b></label>
              <p>lamnguyen20102005@gmail.com</p>

              <button onClick={() => this.openEmployeesPage('Lam')}>Employees</button>
            </div>
          </TabPanel>

          <TabPanel>
            <h3>Employee Information</h3>
            <div className="info-box" id="employeeInfo">
              <table>
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Age</th>
                    <th>Habit</th>
                    <th>Trait</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Jane</td>
                    <td>Smith</td>
                    <td>30</td>
                    <td>Gardening and tending to a vegetable patch.</td>
                    <td>Patient and nurturing, values sustainability.</td>
                  </tr>
                  <tr>
          <td>Sarah</td>
          <td>Williams</td>
          <td>28</td>
          <td>Regularly practicing yoga or Pilates.</td>
          <td>Disciplined and mindful of physical and mental well-being.</td>
        </tr>
        <tr>
          <td>David</td>
          <td>Wilson</td>
          <td>32</td>
          <td>Reading a book before bed every night.</td>
          <td>Imaginative and loves exploring different worlds through literature.</td>
        </tr>
        <tr>
          <td>James</td>
          <td>Jones</td>
          <td>31</td>
          <td>Cooking elaborate meals from scratch.</td>
          <td>Culinary enthusiast, loves experimenting with flavors.</td>
        </tr>
        <tr>
          <td>Daniel</td>
          <td>Taylor</td>
          <td>33</td>
          <td>Keeping a gratitude journal, writing down three things to be grateful for each day.</td>
          <td>Positive and appreciative, focuses on the good in life.</td>
        </tr>
                  {/* More employee rows */}
                </tbody>
              </table>
            </div>
          </TabPanel>

          <TabPanel>
            <h3>Manager Information</h3>
            <div className="info-box">
              <table>
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Jokie</td>
                    <td>Winters</td>
                    <td><a href="javascript:void(0)" className="email-link" onClick={(event) => this.changeEmailLinkColor(event, 'jokiewinters@gmail.com')}>jokiewinters@gmail.com</a></td>
                  </tr>
                  <tr>
                    <td>Sam</td>
                    <td>Robinson</td>
                    <td>
                      <a href="javascript:void(0)" className="email-link" onClick={(event) => this.changeEmailLinkColor(event, 'samrobinson@gmail.com')}>
                        samrobinson@gmail.com
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Emily</td>
                    <td>James</td>
                    <td>
                      <a href="javascript:void(0)" className="email-link" onClick={(event) => this.changeEmailLinkColor(event, 'emilyjames@gmail.com')}>
                        emilyjames@gmail.com
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Mark</td>
                    <td>Thompson</td>
                    <td>
                      <a href="javascript:void(0)" className="email-link" onClick={(event) => this.changeEmailLinkColor(event, 'markthompson@gmail.com')}>
                        markthompson@gmail.com
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Linda</td>
                    <td>Miller</td>
                    <td>
                      <a href="javascript:void(0)" className="email-link managers" onClick={(event) => this.changeEmailLinkColor(event, 'lindamiller@gmail.com')}>
                        lindamiller@gmail.com
                      </a>
                    </td>
                  </tr>
                  {/* More manager rows */}
                </tbody>
              </table>
            </div>
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

export default App;
